import os
import bert.run_classifier as bert_run_classifier


TRAINING_DATA_PATH = os.path.join(os.getcwd(), "data", "training_data")
PROCESSED_DATA_PATH = os.path.join(os.getcwd(), "data", "processed")

test_user_ids = [18482, 4606, 14574, 49714, 9583, 9212, 36890, 152, 25499,
                 22006, 46315, 18209, 32625, 2435, 38629, 35398, 50464, 40448,
                 50571, 13018, 51023, 11978, 10701, 5611, 38533, 35751, 2071,
                 38379, 10835, 19512, 15979, 40703, 48461, 47190, 14542, 41059,
                 5985, 48007, 36977, 8554, 10276, 50604, 7662, 5548, 49677,
                 39330, 16955, 10753, 10400, 5827, 47415, 29216, 22733, 14406,
                 15696, 33250, 48723, 16723, 31370, 45509, 50294, 45351, 40756,
                 37840, 10287, 33620, 5169, 29478, 2667, 27495, 32063, 48651,
                 5215, 22354, 56, 26545, 46485, 19871, 1215, 41711, 15000,
                 19994, 3402, 42355, 48276, 1690, 5196, 8967, 45222, 5096,
                 4796, 40823, 10869, 16392, 44265, 11534, 44747, 13704, 39474,
                 19955]


def generate_train_test_labels():

    crowd_train = pd.read_csv(os.path.join(TRAINING_DATA_PATH, "crowd_train.csv"))
    crowd_train = crowd_train.dropna()

    crowd_train_all_users = crowd_train['user_id']
    train_user_ids = list(set(crowd_train_all_users) - set(test_user_ids))

    train_labels = pd.DataFrame(columns=['user_id'])
    test_labels = pd.DataFrame(columns=['user_id'])

    train_labels['user_id'] = train_user_ids
    test_labels['user_id'] = test_user_ids

    train_labels = pd.merge(crowd_train, train_labels, on=['user_id'], how='right')
    test_labels = pd.merge(crowd_train, test_labels, on=['user_id'], how='right')

    train_labels.to_csv(os.path.join(PROCESSED_DATA_PATH, "crowd_train_labels.csv"))
    test_labels.to_csv(os.path.join(PROCESSED_DATA_PATH, "crowd_test_labels.csv"))



def generate_BERT_format_csv(per_user_text):

    """
    :param per_user_text: This is a csv file with two columns: user_id, all_text. It has all the texts for each user.
    This csv is obtained from process.py -> generate_per_user_text()

    """

    train_labels = pd.read_csv(os.path.join(PROCESSED_DATA_PATH, "crowd_train_labels.csv"))
    test_labels = pd.read_csv(os.path.join(PROCESSED_DATA_PATH, "crowd_test_labels.csv"))

    train_info = pd.merge(per_user_text, train_labels, on=['user_id'], how='right')
    train_sentences = train_info['all_text']

    test_info = pd.merge(per_user_text, test_labels, on=['user_id'], how='right')
    test_sentences = test_info['all_text']

    train_bert_format = pd.DataFrame(columns = ['id' , 'label', 'letter', 'text'])
    test_bert_format = pd.DataFrame(columns = ['id', 'sentence'])

    train_bert_format['id'] = train_labels['user_id']
    train_bert_format['label'] = train_labels['raw_label']
    train_bert_format['letter'] = 'a'  # this will be ignored
    train_bert_format['text'] = train_sentences

    test_bert_format['id'] = test_labels['user_id']
    test_bert_format['sentence'] = test_sentences


    train_bert_format.to_csv(os.path.join(PROCESSED_DATA_PATH, "train_BERT_finetune_format.csv"))
    test_bert_format.to_csv(os.path.join(PROCESSED_DATA_PATH, "test_BERT_finetune_format.csv"))


def convert_bert_csv_to_tsv():

    train_bert_format_csv = pd.read_csv(os.path.join(PROCESSED_DATA_PATH, "train_BERT_finetune_format.csv"))
    test_bert_format_csv = pd.read_csv(os.path.join(PROCESSED_DATA_PATH, "test_BERT_finetune_format.csv"))


    train_bert_format_csv.to_csv(os.path.join(PROCESSED_DATA_PATH, 'train_BERT_finetune_format.tsv'), sep='\t', index=False, header=False)
    test_bert_format_csv.to_csv(os.path.join(PROCESSED_DATA_PATH, 'test_BERT_finetune_format.tsv'), sep='\t', index=False, header=True)


def BERT_run_classifier():

    flag_inputs = {}


    flag_inputs['DATA_DIR']= os.path.join(PROCESSED_DATA_PATH, "data")
    flag_inputs['BERT_CONFIG_FILE'] = os.path.join(os.getcwd(), "uncased_L-12_H-768_A-12/bert_config.json")
    flag_inputs['TASK_NAME'] = "cola"
    flag_inputs['VOCAB_FILE']= os.path.join(os.getcwd(), "uncased_L-12_H-768_A-12/vocab.txt")
    flag_inputs['OUTPUT_DIR']= os.path.join(PROCESSED_DATA_PATH, "bert_finetune_output")
    flag_inputs['INIT_CHECKPOINT'] = os.path.join(os.getcwd(), "uncased_L-12_H-768_A-12/bert_model.ckpt")
    flag_inputs['DO_LOWER_CASE'] = True
    flag_inputs['MAX_SEQ_LENGTH'] = 128
    flag_inputs['DO_TRAIN'] = False
    flag_inputs['DO_EVAL'] = False
    flag_inputs['DO_PREDICT'] = False
    flag_inputs['TRAIN_BATCH_SIZE'] = 32
    flag_inputs['EVAL_BATCH_SIZE'] = 8
    flag_inputs['PREDICT_BATCH_SIZE'] = 8
    flag_inputs['LEARNING_RATE'] = 5e-5
    flag_inputs['NUM_TRAIN_EPOCHS'] = 3.0
    flag_inputs['WARMUP_PROPORTION'] = 0.1
    flag_inputs['SAVE_CHECKPOINTS_STEPS'] = 1000
    flag_inputs['ITERATIONS_PER_LOOP'] = 1000
    flag_inputs['USE_TPU'] = False
    flag_inputs['TPU_NAME'] = None
    flag_inputs['TPU_ZONE'] = None
    flag_inputs['GCP_PROJECT'] = None
    flag_inputs['MASTER'] = None
    flag_inputs['NUM_TPU_CORES'] = 8

    print("calling run_classifier.py")
    bert_run_classifier.main(flag_inputs)



def main():
    generate_train_test_labels()

    per_user_text = pd.read_csv(os.path.join(PROCESSED_DATA_PATH, "all_data_per_user.csv"))
    generate_BERT_format_csv(per_user_text)
    convert_bert_csv_to_tsv()
    BERT_run_classifier()
