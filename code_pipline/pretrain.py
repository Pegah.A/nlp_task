from pathlib import Path
import pandas as pd
from spacy.lang.en import English
import numpy as np
import os
import jsonlines
import math

#----
import sys
import csv
maxInt = sys.maxsize

while True:
    # decrease the maxInt value by factor 10
    # as long as the OverflowError occurs.

    try:
        csv.field_size_limit(maxInt)
        break
    except OverflowError:
        maxInt = int(maxInt/10)

#----


import bert.create_pretraining_data as bert_create_pretraining_data
import bert.run_pretraining as bert_run_pretraining


TRAINING_DATA_PATH = os.path.join(os.getcwd(), "data", "training_data")
PROCESSED_DATA_PATH = os.path.join(os.getcwd(), "data", "processed")

def BERT_creating_pretraining_data():

    flag_inputs = {}

    flag_inputs['INPUT_FILE'] = os.path.join(PROCESSED_DATA_PATH, "task_A_training_data_sentences.txt")
    flag_inputs['OUTPUT_FILE'] = os.path.join(PROCESSED_DATA_PATH, "BERT_tf_examples.tfrecord")
    flag_inputs['VOCAB_FILE'] = os.path.join(os.getcwd(), "uncased_L-12_H-768_A-12/vocab.txt")
    flag_inputs['DO_LOWER_CASE'] = True
    flag_inputs['MAX_SEQ_LENGTH'] = 128
    flag_inputs['MAX_PREDICTIONS_PER_SEQ'] = 20
    flag_inputs['RANDOM_SEED'] = 12345
    flag_inputs['DUPE_FACTOR'] = 10
    flag_inputs['MASKED_LM_PROB'] = 0.15
    flag_inputs['SHORT_SEQ_PROB'] = 0.1


    print ("calling BERT create pre-training data.py")
    bert_create_pretraining_data.main(flag_inputs)

def BERT_run_pretraining():


    flag_inputs = {}

    flag_inputs['INPUT_FILE'] = DATA_INPUT_DIR
    flag_inputs['OUTPUT_DIR'] = DATA_OUTPUT_DIR
    flag_inputs['BERT_CONFIG_FILE'] = DATA_OUTPUT_DIR
    flag_inputs['INIT_CHECKPOINT'] = True
    flag_inputs['MAX_SEQ_LENGTH'] = 128
    flag_inputs['MAX_PREDICTIONS_PER_SEQ'] = 20
    flag_inputs['DO_TRAIN'] = False
    flag_inputs['DO_EVAL'] = False
    flag_inputs['TRAIN_BATCH_SIZE'] = 32
    flag_inputs['EVAL_BATCH_SIZE'] = 8
    flag_inputs['LEARNING_RATE'] = 5e-5
    flag_inputs['NUM_TRAIN_STEPS'] = 100000
    flag_inputs['NUM_WARMUP_STEPS'] = 10000
    flag_inputs['SAVE_CHECKPOINTS_STEPS'] = 1000
    flag_inputs['ITERATIONS_PER_LOOP'] = 1000
    flag_inputs['MAX_EVAL_STEPS'] = 100
    flag_inputs['USE_TPU'] = False
    flag_inputs['TPU_NAME'] = None
    flag_inputs['TPU_ZONE'] = None
    flag_inputs['GCP_PROJECT'] = None
    flag_inputs['MASTER'] = None
    flag_inputs['NUM_TPU_CORES'] = 8


    print ("calling BERT run_pretraining.py")
    bert_run_pretraining.main(flag_inputs)
    

def main():
    print ("hi")

if __name__ == "__main__":

    print ("hello")

