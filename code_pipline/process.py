 
import re    
import pandas as pd
from spacy.lang.en import English
import os 
import math
    
current_path = "/Users/pegah_abed/Documents/BERT"
TRAINING_DATA_PATH = os.path.join(current_path, "data", "training_data")
PROCESSED_DATA_PATH = os.path.join(current_path, "data", "processed")
ALL_POSTS_PATH = os.path.join(TRAINING_DATA_PATH, "shared_task_posts.csv")
ALL_POSTS_PROCESSED_PATH = os.path.join(PROCESSED_DATA_PATH, "all_posts_processed.csv")


MAX_POST_LENGTH = 5000
CHUNK_SIZE = 500000



def get_all_posts():
    """
    Get all the shared task posts
    """
    all_posts = pd.read_csv(ALL_POSTS_PATH)
    return all_posts


def merge_post_title_and_body(posts, posts_processed_path):
    """
    :param posts:  csv file containing the posts info
    :param posts_processed_path: the path to save the new csv file after merging post_title and post_body columns

     Merge post title and post body of posts into a new column called all_text
    """

    posts.post_title = posts.post_title.fillna('')
    posts.post_body = posts.post_body.fillna('')
    posts.loc[~posts.post_title.str.endswith('.'), 'post_title'] = posts.loc[~posts.post_title.str.endswith('.'), 'post_title'] + '.'
    posts['all_text'] = posts['post_title'] + ' ' + posts['post_body']
    
    posts.to_csv(posts_processed_path)

    
def get_all_posts_processed():
    """
    This will return the all_posts_new.csv which has the post title and body merged.
    """
    all_posts_processed = pd.read_csv(os.path.join(PROCESSED_DATA_PATH, "all_posts_new.csv"))
    return all_posts_processed
    
def remove_person_tags(posts, posts_path):
    """
    To replace repeated PERSON tags with just one tag.
    This implementation is very very slow.
    Can you think of a better way? Vectorization?
    """

    all_texts = posts['all_text']


    # using regular expressions
    for i in range(len(all_texts)):
        all_texts[i] = re.sub("_(PERSON_)+", "_PERSON_", all_texts[i])
        
    posts['all_text'] = all_texts
    
    posts.to_csv(posts_path)
    
    
def remove_large_posts(posts):
    """
    Removes posts that are larger than MAX_POST_LENGTH
    """

    
    file_name = "removed_posts_larger_than_" + str(MAX_POST_LENGTH) + ".csv"
    removed_posts_path = os.path.join(PROCESSED_DATA_PATH, file_name)
    removed_posts_df = pd.DataFrame(columns = ['post_id'])
    
    file_name = "all_posts_except_larger_than_" + str(MAX_POST_LENGTH) + ".csv"
    all_posts_except_long_ones_path = os.path.join(PROCESSED_DATA_PATH, file_name)
    
    removed_posts_list =[]
    
    for i in range(len(posts)):
        if len(posts['all_text'].iloc[i]) > MAX_POST_LENGTH:
            removed_posts_list.append(posts['post_id'].iloc[i])
            
    print ("Number of removed posts: ", len(removed_posts_list))
    removed_posts_df['post_id'] = removed_posts_list
    removed_posts_df.to_csv(removed_posts_path)
    
    posts = posts[~posts.post_id.isin(removed_posts_list)]
    posts.to_csv(all_posts_except_long_ones_path)


def get_csv_of_posts_smaller_than_threshold(threshold):
    """
    Returns the csv file corresponding to the posts that are smaller than threshold.
    """

    file_name = "all_posts_except_larger_than_" + str(threshold) + ".csv"
    posts = pd.read_csv(os.path.join(PROCESSED_DATA_PATH, file_name))
    return posts


def generate_per_user_text(posts):

    text_per_user_path = os.path.join(PROCESSED_DATA_PATH, "all_data_per_user.csv")

    # drop all columns except user_id and all_text  (all_text is post_title merged with post_body)
    posts = posts.drop(columns=['post_id', 'timestamp', 'subreddit', 'post_title', 'post_body'])

    print (posts.head())
    posts = posts.groupby('user_id')['all_text'].apply(lambda x: "%s" % ' '.join(x))

    posts.to_csv(text_per_user_path)


def generate_task_posts(csv_file, task):

    """
    :param csv_file: This is the csv file that we use to get all the posts.
    It could be all the posts or the ones that are smaller than MAX_POST_LENGTH
    :param task: a string that will determine which task posts we want (A or B or C)

    This function will crate a new csv file that contains the post info of the requested task.
    """
    file_name = "task_" + task + "_train_posts.csv"
    task_train_posts_path = os.path.join(PROCESSED_DATA_PATH, file_name)

    all_posts = csv_file
    all_posts.post_title = all_posts.post_title.fillna('')
    all_posts.post_body = all_posts.post_body.fillna('')

    task_train_post_info = pd.read_csv(task_train_path)  # this csv has columns: post_id, user_id, subreddit

    # merge it with all_posts csv file to get the desired columns
    task_train_posts = pd.merge(all_posts, task_train_post_info[['post_id']], on=['post_id'], how='right')

    # merging post_title and post_body
    task_train_posts.loc[~task_train_posts.post_title.str.endswith('.'), 'post_title'] = task_train_posts.loc[
                                                                                                 ~task_train_posts.post_title.str.endswith(
                                                                                                     '.'), 'post_title'] + '.'
    task_train_posts["post_title_and_body"] = task_train_posts["post_title"] + " " + task_train_posts["post_body"]
    task_train_posts.to_csv(task_train_posts_path)

    return task_train_posts


def process_sentences(csv_file, task = None):
    """
    Creates a new csv file in which each row represents a sentence.
    The input csv file could be all the posts or the post of a specific task.
    """
    
    posts = csv_file
    
    # simplify NLP pipeline to detect sentence boundaries
    nlp=English()
    sbd = nlp.create_pipe('sentencizer')
    nlp.add_pipe(sbd)
    
    sentences = generate_sentence_df(df=posts, text_col='all_text', post_id_col='post_id')
    print("Writing all text sentences...")

    if task is None:
        sentences.to_csv(os.path.join(PROCESSED_DATA_PATH, "training_data_sentences.csv"), index=None)
    else:
        file_name = "task_" + task + "_training_data_sentences.csv"
        sentences.to_csv(os.path.join(PROCESSED_DATA_PATH, file_name), index=None)

    

def generate_sentences_txt_file(task=None):
    """
    Creates a new txt file in which each row represents a sentence.
    The input csv file could be all the sentences or the sentences of a specific task.
    """

    if task is None:
        csv_file = pd.read_csv(os.path.join(PROCESSED_DATA_PATH, "training_data_sentences.csv"))
        training_txt_path = os.path.join(PROCESSED_DATA_PATH, "training_data_sentences.txt")
    else:
        file_name = "task_" + task + "_training_data_sentences"

        csv_file = pd.read_csv(os.path.join(PROCESSED_DATA_PATH, file_name + ".csv"))
        training_txt_path = os.path.join(PROCESSED_DATA_PATH, file_name + ".txt")

    training_data_sentences_txt = open(training_txt_path, "w")

    body = csv_file['all_text']
    for item in body:
        training_data_sentences_txt.write(str(item))
        training_data_sentences_txt.write("\n")
    

    
def break_into_chunks():
    """
    Breaking the training_data_sentences.txt into smaller chunks of size CHUNK_SIZE
    """

    training_data_path = os.path.join(PROCESSED_DATA_PATH, "training_data_sentences.txt")
    training_data = open(training_data_path, "r")
    lines = training_data.readlines()
    
    number_of_lines = len(lines)
    number_of_chunks = math.ceil(number_of_lines / CHUNK_SIZE)
    
    range_start = 0
    range_end = range_start + CHUNK_SIZE
    for i in range( 1, number_of_chunks +1 ):
        
        chunk_path = os.path.join(PROCESSED_DATA_PATH, "all_data_chunk_" + str(i) +".txt")
        chunk_file = open(chunk_path, "w")

        if i == number_of_chunks:
            range_end = number_of_lines
            
        for j in range(range_start , range_end):
            chunk_file.write(lines[j])
            
        range_start = range_end
        range_end = range_start + CHUNK_SIZE 

def main():

    all_posts = get_all_posts()
    merge_post_title_and_body(all_posts, ALL_POSTS_PROCESSED_PATH)

    all_posts_processed = get_all_posts_processed()
    remove_person_tags(all_posts_processed, ALL_POSTS_PROCESSED_PATH)

    all_posts_processed = get_all_posts_processed()
    remove_large_posts(all_posts_processed)

    all_posts_except_large_ones = get_csv_of_posts_smaller_than_threshold(MAX_POST_LENGTH)

    generate_per_user_text(all_posts_except_large_ones)

    task_A_posts = generate_task_posts(all_posts_except_large_ones, "A")
    task_B_posts = generate_task_posts(all_posts_except_large_ones, "B")
    task_C_posts = generate_task_posts(all_posts_except_large_ones, "C")

    process_sentences(all_posts_except_large_ones)
    process_sentences(task_A_posts, "A")
    process_sentences(task_B_posts, "B")
    process_sentences(task_C_posts, "C")

    generate_sentences_txt_file(task=None)
    generate_sentences_txt_file("A")
    generate_sentences_txt_file("B")
    generate_sentences_txt_file("C")

    break_into_chunks()

