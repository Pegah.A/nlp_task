from pathlib import Path
import pandas as pd
from spacy.lang.en import English
import numpy as np
import os
import sys
import jsonlines

sys.path.append(os.getcwd())
import bert.extract_features as bert_extract_features

TRAINING_DATA_PATH = os.path.join(os.getcwd(), "data", "training_data")
PROCESSED_DATA_PATH = os.path.join(os.getcwd(), "data", "processed")
ALL_POSTS_PATH = os.path.join(TRAINING_DATA_PATH, "shared_task_posts.csv")
ALL_POSTS_PROCESSED_PATH = os.path.join(PROCESSED_DATA_PATH, "all_posts_processed.csv")


CHUNK_SIZE = 500000

def flatten_cols(df): 
    df.columns = [
        '_'.join(tuple(map(str, t))).rstrip('_')
        for t in df.columns.values
    ]
    return df

def get_training_data_sentences():
    
    training_data_sentences = pd.read_csv(os.path.join(PROCESSED_DATA_PATH, "training_data_sentences.csv"))
    return training_data_sentences

    

def extract_BERT_features():
    """
    This fucntion calls the main function in the BERT extract_features.py
    """

    flag_inputs = {}

    # # you have to modify this part based on your files.
    flag_inputs['INPUT_FILE'] = os.path.join(PROCESSED_DATA_PATH, "training_data_sentences.txt")
    flag_inputs['OUTPUT_FILE'] = os.path.join(PROCESSED_DATA_PATH,"BERT_output.jsonl")

    flag_inputs['VOCAB_FILE'] = os.path.join(os.getcwd(),"uncased_L-12_H-768_A-12/vocab.txt")
    flag_inputs['LAYERS'] = "-1"
    flag_inputs['BERT_CONFIG_FILE'] = os.path.join(os.getcwd(),"uncased_L-12_H-768_A-12/bert_config.json")
    flag_inputs['INIT_CHECKPOINT'] = os.path.join(os.getcwd(),"uncased_L-12_H-768_A-12/bert_model.ckpt")
    flag_inputs['BATCH_SIZE'] = 8
    flag_inputs['MAX_SEQ_LENGTH'] = 128
    flag_inputs['DO_LOWER_CASE'] = True
    flag_inputs['USE_TPU'] = False
    flag_inputs['MASTER'] = None
    flag_inputs['USE_ONE_HOT_EMBEDDINGS'] = False

    bert_extract_features.main(flag_inputs)


def process_BERT_output():
    
    
    # you have to modify this part based on your file.
    # if you have chunks, set in_chunks to True and provide the chunk_num otherwise, set in_chunks to False
    # the input file should be .jsonl format
    BERT_output_file_name = "BERT_output.jsonl"
    BERT_features_per_sentence_file_name = "BERT_features_per_sentence.csv"
    in_chunks = True
    chunk_num = 13
    
    #----------------------------
    
    
    bert_features_csv_path = os.path.join(PROCESSED_DATA_PATH, BERT_features_per_sentence_file_name)
    bert_features_list = []
    bert_output_path = os.path.join(PROCESSED_DATA_PATH, BERT_output_file_name)
    with jsonlines.open(bert_output_path) as reader:
        for line in reader:
            cls_token = line['features'][0]
            cls_token_embedding = cls_token['layers'][0]['values']  # index 0 means these are from the last layer
            bert_features_list.append(cls_token_embedding)

    bert_features_df = pd.DataFrame(bert_features_list)
    
    training_data_sentences = get_training_data_sentences()
    
    if in_chunks:
        
        range_start = (chunk_num - 1) * CHUNK_SIZE
        range_end = range_start + CHUNK_SIZE
        
        if range_end > len(training_data_sentences):
            range_end = len(training_data_sentences)
        
        training_data_sentences = training_data_sentences.loc[range_start:range_end].reset_index()
   
    
    bert_features_df['post_id'] = training_data_sentences['post_id']
    bert_features_df['sentence_num'] = training_data_sentences['sentence_num']
    
    cols = bert_features_df.columns.tolist()
    cols = cols[-2:] + cols[:-2]
    bert_features_df = bert_features_df[cols]
    
    
    bert_features_df.to_csv(bert_features_csv_path, index = False)


def aggregate_BERT_features_per_post():

    """
    The bert_features.csv files has the following columns:
    post_id / sentence_num / 768 columns as featuresa

    In this function, I will use the post_id to pick the sentences that belong to the same post
    and aggregate the corresponding 786 features.
    I will use min, max, and mean.

    I am saving them all in one csv file.
    Therefore, there will be 1 csv file with 1 + (768 *3) columns and #rows == #posts
    """

    # you have to modify this part based on your file names
    BERT_features_per_sentence_file_name = "chunk_13_BERT_features_per_sentence.csv"
    BERT_features_per_post_file_name = "chunk_13_BERT_features_per_post.csv"
                            
    # ---------------------- 
                            
    bert_features_per_post_path = os.path.join(PROCESSED_DATA_PATH, BERT_features_per_post_file_name)
    bert_features_per_sentence = pd.read_csv(os.path.join(PROCESSED_DATA_PATH, BERT_features_per_sentence_file_name))
    
    print (bert_features_per_sentence.head())

    bert_features_per_sentence = bert_features_per_sentence.drop(columns= ['sentence_num'])

    bert_features_per_post = flatten_cols(bert_features_per_sentence.groupby(['post_id']).agg(['mean', 'max', 'min']))
    bert_features_per_post.to_csv(bert_features_per_post_path)


def aggregate_BERT_features_per_user():
                            
     # you have to modify this part based on your file names
    BERT_features_per_sentence_file_name = "chunk_13_BERT_features_per_sentence.csv"
    BERT_features_per_user_file_name = "chunk_13_BERT_features_per_user.csv"
    # ----------------------                         

    bert_features_per_user_path = os.path.join(PROCESSED_DATA_PATH, BERT_features_per_user_file_name)
    bert_features_per_sentence = pd.read_csv(os.path.join(PROCESSED_DATA_PATH, BERT_features_per_sentence_file_name))
    bert_features_per_sentence = bert_features_per_sentence.drop(columns=['sentence_num'])

    task_A_train_posts = pd.read_csv(os.path.join(os.getcwd(), "data/processed/task_A_train_posts.csv"))

    # add the user_id column
    bert_features_per_sentence= pd.merge(bert_features_per_sentence, task_A_train_posts[['post_id', 'user_id']],
                                      on=['post_id'], how='left')

    # change the order of the columns to make user_id the first
    cols = bert_features_per_sentence.columns.tolist()
    cols = cols[-1:] + cols[:-1]
    bert_features_per_sentence = bert_features_per_sentence[cols]


    bert_features_per_user = flatten_cols(bert_features_per_sentence.groupby(['user_id']).agg(['mean', 'max', 'min']))
    bert_features_per_user.to_csv(bert_features_per_user_path)


def concatenate_chunk_csv_files_into_one():
    print("Concatenating all the chunks together...")
    BERT_features_per_post_all_data_path = os.path.join(PROCESSED_DATA_PATH, "BERT_features_per_post_all_data.csv")
    chunks_list = []
    for i in range(1, 14):
        chunk_file = pd.read_csv(os.path.join(PROCESSED_DATA_PATH, "chunk_" + str(i) + "_BERT_features_per_post.csv"))
        chunks_list.append(chunk_file)

    BERT_features_per_post_all_data = pd.concat(chunks_list, ignore_index=True)
    BERT_features_per_post_all_data.to_csv(BERT_features_per_post_all_data_path)

    print("Concatenating chunks finished.")




    

def main():
    print ("hello0o")

if __name__ == "__main__":

    
    
    #training_data_sentences = get_training_data_sentences()
    #training_data_sentences = training_data_sentences[:10]
    #convert_training_sentences_to_txt()
    #convert_task_A_sentences_to_txt()


    
    #extract_BERT_features()
    #process_BERT_features()
    #aggregate_BERT_features_per_post()
    #temp_function_to_check_csv_sentence_files()
    #temp_function_to_check_csv_post_files()
                                                        
                                                        
    #concatenate_chunk_csv_files_into_one()
    temp_function_to_check_big_file()



