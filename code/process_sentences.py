import re
from pathlib import Path
import pandas as pd
from spacy.lang.en import English
import os 


RAW_DATA = Path('./data/clpsych19_training_data/')
PROCESSED_PATH = Path('./data/processed')
PROCESSED_PATH.mkdir(exist_ok=True)

def generate_sentence_df(df, text_col, post_id_col):
    df['parsed_text'] = df[text_col].astype(str).apply(nlp)
    corpus_dict = {}
    for i, doc in df.iterrows():
        document_dict = {}
        post_id = doc[post_id_col]
        parsed_doc = doc['parsed_text']
        for i, sentence in enumerate(parsed_doc.sents):
            #replace one or more person tags - haven't been ran but it's something Leon might try
            #sentence = re.sub("_(PERSON_)+", "_PERSON_", sentence)
            #remove underscores at end of sentence
            #sentence = re.sub("_$", "", sentence)
            document_dict[i] = str(sentence)
        corpus_dict[post_id] = document_dict
        
    sentences = pd.DataFrame(corpus_dict).T.reset_index().rename(columns={'index': 'post_id'})
    sentences = pd.melt(sentences, id_vars='post_id', var_name='sentence_num', value_name='body')
    sentences = sentences.dropna().sort_values(['post_id', 'sentence_num'])
    
    return sentences


if __name__ == '__main__':
    all_posts = pd.read_csv(os.path.join(PROCESSED_PATH, "all_posts_except_larger_than_5000.csv"))
    all_posts.post_title = all_posts.post_title.fillna('')
    all_posts.post_body = all_posts.post_body.fillna('')
    all_posts.loc[~all_posts.post_title.str.endswith('.'), 'post_title'] = all_posts.loc[~all_posts.post_title.str.endswith('.'), 'post_title'] + '.'
    all_posts['all_text'] = all_posts['post_title'] + ' ' + all_posts['post_body']
    # simplify NLP pipeline to detect sentence boundaries
    nlp=English()
    sbd = nlp.create_pipe('sentencizer')
    nlp.add_pipe(sbd)
    
    sentences = generate_sentence_df(df=all_posts, text_col='all_text', post_id_col='post_id')
    print(f"Writing all text sentences (post_title + post_body) to {PROCESSED_PATH / 'training_data_sentences_except_larger_than_5000.csv'}")
    sentences.to_csv(PROCESSED_PATH / 'training_data_sentences_except_larger_than_5000.csv', index=None)