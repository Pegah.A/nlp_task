from pathlib import Path
import pandas as pd
from spacy.lang.en import English
import numpy as np
import os
import sys
import jsonlines


#sys.path.append(os.getcwd()+ os.sep + os.pardir)
sys.path.append(os.getcwd())
#import bert.create_pretraining_data as bert_create_pretraining_data
import bert.run_pretraining as bert_run_pretraining

data_path = os.path.join(os.getcwd(), "data", "training_data")
processed_data_path = os.path.join(os.getcwd(), "data", "processed")

def BERT_creating_pretraining_data():

    print ("calling BERT create pretraining data.py")
    bert_create_pretraining_data.main()

def BERT_run_pretraining():
    print ("calling BERT run_pretraining.py")
    bert_run_pretraining.main()
    

    
def prepare_finetune_format():
    
    labels = pd.read_csv(os.path.join())
    BERT_finetune_df = pd.DataFrame(columns = ['id', 'label', 'letter', 'text'])
    
    

if __name__ == "__main__":

    


    #training_data_sentences = get_training_data_sentences()
    #training_data_sentences = training_data_sentences[:10]
    #convert_training_sentences_to_txt(training_data_sentences)
    #convert_task_A_sentences_to_txt()


    
    #extract_BERT_features()
    
    #process_BERT_features()
    #aggregate_BERT_features_per_post()

    #BERT_creating_pretraining_data()
    BERT_run_pretraining()



