from pathlib import Path
import pandas as pd
from spacy.lang.en import English
import numpy as np
import os
import sys
import re
#import jsonlines




data_path = os.path.join(os.getcwd(), "data", "training_data")
processed_data_path = os.path.join(os.getcwd(), "data", "processed")
all_posts_path = os.path.join(data_path, "shared_task_posts.csv")
task_A_train_path = os.path.join(data_path, "task_A_train.posts.csv")
task_B_train_path = os.path.join(data_path, "task_B_train.posts.csv")
task_C_train_path = os.path.join(data_path, "task_C_train.posts.csv")




def generate_task_A_train_data_posts():

    """
    training data of task A. Columns are: post_id / user_id / timestamp / subreddit / post_title / post_body
    number of rows == 919
    """

    task_A_train_posts_path = os.path.join(processed_data_path, "task_A_train_posts.csv")
    all_posts = pd.read_csv(all_posts_path)
    task_A_train_post_info = pd.read_csv(task_A_train_path)
    task_A_train_posts = pd.merge(all_posts, task_A_train_post_info[['post_id']], on=['post_id'], how='right')

    # merging post_title and post_body
    task_A_train_posts = task_A_train_posts.fillna("")  # replacing  NaN values with empty string
    task_A_train_posts["post_title_and_body"] = task_A_train_posts["post_title"] + " " + task_A_train_posts["post_body"]

    task_A_train_posts.to_csv(task_A_train_posts_path)

def generate_task_A_train_data_sentences():
    
    training_data_sentences = get_training_data_sentences()
    task_A_training_posts = pd.read_csv(os.path.join(processed_data_path, "task_A_train_posts.csv"))
    
    task_A_training_data_sentences = pd.merge(training_data_sentences, task_A_training_posts[['post_id']], on=['post_id'], how='right')
    
    
    task_A_training_data_sentences.to_csv(os.path.join(processed_data_path, "task_A_training_data_sentences.csv"), index = False)

def task_A_train_data_stats():

    task_A_train_posts_path = os.path.join(processed_data_path, "task_A_train_posts.csv")
    task_A_train_posts = pd.read_csv(task_A_train_posts_path)

    print ("number of unique posts IDs: ", task_A_train_posts['post_id'].nunique())
    print ("number of unique user IDs ", task_A_train_posts['user_id'].nunique())

    post_title_and_body = task_A_train_posts['post_title_and_body']

    over_128_count  = 0
    over_512_count = 0

    for i in range(len(post_title_and_body)):
        num_of_words = len(post_title_and_body.iloc[i].split(" "))

        if num_of_words > 512:
            over_512_count += 1

        if num_of_words > 128:
            over_128_count += 1

    print ("over 512 count is: ", over_512_count)
    print ("over 128 count is: ", over_128_count)

    
def get_training_data_sentences():
    training_data_sentences_path = os.path.join(os.getcwd(), "data/processed/training_data_sentences.csv")
    training_data_sentences = pd.read_csv(training_data_sentences_path)

    return training_data_sentences




def convert_training_sentences_to_txt():

    training_data_sentences = get_training_data_sentences()
    body = training_data_sentences['body']
    training_txt_path = os.path.join(os.getcwd(), "data/processed/training_data_sentences.txt")
    training_data_sentences_txt = open(training_txt_path, "w")

    for item in body:
        training_data_sentences_txt.write(str(item))
        training_data_sentences_txt.write("\n")
        
def convert_task_A_sentences_to_txt():
    
    task_A_training_data_sentences = pd.read_csv(os.path.join(os.getcwd(), "data/processed/task_A_training_data_sentences.csv"))
    body = task_A_training_data_sentences['body']
    task_A_training_txt_path = os.path.join(os.getcwd(), "data/processed/task_A_training_data_sentences.txt")
    task_A_training_data_sentences_txt = open(task_A_training_txt_path, "w")

    for item in body:
        task_A_training_data_sentences_txt.write(str(item))
        task_A_training_data_sentences_txt.write("\n")
        
def remove_person_tags():
    training_data_sentences_txt_path = os.path.join(os.getcwd(), "data/processed/training_data_sentences.txt")
    training_data_sentences_with_removed_person_tags_txt_path = os.path.join(os.getcwd(), "data/processed/training_data_sentences_with_removed_person_tags.txt")
    
    training_data_sentences_txt = open(training_data_sentences_txt_path, "r")
    training_data_sentences_with_removed_person_tags_txt = open(training_data_sentences_with_removed_person_tags_txt_path, "w")
    
    lines = training_data_sentences_txt.readlines()
    for line in lines:
        line = re.sub("_(PERSON_)+", "_PERSON_", line)  
        training_data_sentences_with_removed_person_tags_txt.write(line)
        
        
def check_post_length():
    training_data_sentences_with_removed_person_tags_txt_path = os.path.join(os.getcwd(), "data/processed/training_data_sentences_with_removed_person_tags.txt")
    training_data_sentences_with_removed_person_tags_txt = open(training_data_sentences_with_removed_person_tags_txt_path, "r")
    
    lines = training_data_sentences_with_removed_person_tags_txt.readlines()
    print ("number of lines in the txt file is: ", len(lines))
    
    max_len = 0
    for i in range(len(lines)):
        if len(lines[i]) > 100000:
            print ("at index ", i)
        if len(lines[i]) > max_len:
            max_len = len(lines[i])
            
    print ("max len is: ", max_len)
    
  

def remove_large_posts():
    
    removed_posts_path = os.path.join(processed_data_path, "removed_posts_larger_than_5000.csv")
    removed_posts_df = pd.DataFrame(columns = ['post_id'])
    
    all_posts_except_long_ones_path = os.path.join(processed_data_path, "all_posts_except_larger_than_5000.csv")
    
    all_posts = pd.read_csv(all_posts_path)
    
    print ("total number of posts is: ", len(all_posts))
    number_of_unique_users = all_posts['user_id'].nunique()
    print ("number of unique users", number_of_unique_users)

    all_posts.post_title = all_posts.post_title.fillna('')
    all_posts.post_body = all_posts.post_body.fillna('')
    all_posts.loc[~all_posts.post_title.str.endswith('.'), 'post_title'] = all_posts.loc[~all_posts.post_title.str.endswith('.'), 'post_title'] + '.'
    
    print ("creating a new column to concat post title and post body...")
    all_posts['all_text'] = all_posts['post_title'] + ' ' + all_posts['post_body']
    
    removed_posts_list =[]
    
    for i in range(len(all_posts)):
        if len(all_posts['all_text'].iloc[i]) > 5000:
            removed_posts_list.append(all_posts['post_id'].iloc[i])
   
    removed_posts_df['post_id'] = removed_posts_list
    removed_posts_df.to_csv(removed_posts_path)
    
    all_posts = all_posts[~all_posts.post_id.isin(removed_posts_list)]
    all_posts.to_csv(all_posts_except_long_ones_path)
    
    print ("number of removed posts: ", len(removed_posts_list))
    print ("number of posts after removing the long ones: ", len(all_posts))
    number_of_unique_users = all_posts['user_id'].nunique()
    print ("number of unique users", number_of_unique_users)
    
    
    
    

if __name__ == "__main__":

    #generate_task_A_train_data_posts()
    #task_A_train_data_stats()
    #make_BERT_task_A_training_input_sentence_level_txt()
    #training_data_sentences = get_training_data_sentences()
    #training_data_sentences = training_data_sentences[:10]
    #convert_training_sentences_to_txt(training_data_sentences)
    #process_BERT_features()
    #generate_task_A_train_data_sentences()
    #remove_person_tags()
    
    remove_large_posts()
    


