from pathlib import Path
import pandas as pd
from spacy.lang.en import English
import numpy as np
import os
import sys
import jsonlines

sys.path.append(os.getcwd())
import bert.extract_features as bert_extract_features



def flatten_cols(df): 
    df.columns = [
        '_'.join(tuple(map(str, t))).rstrip('_')
        for t in df.columns.values
    ]
    return df

def get_training_data_sentences():
    training_data_sentences_path = os.path.join(os.getcwd(), "data/processed/training_data_sentences.csv")
    training_data_sentences = pd.read_csv(training_data_sentences_path)

    return training_data_sentences

    

def extract_BERT_features():
    bert_extract_features.main()


def process_BERT_features():

    bert_features_csv_path = os.path.join(os.getcwd(), "data/processed/task_A_BERT_features_per_sentence_pretrained_on_A.csv")

    bert_features_list = []
    bert_output_path = os.path.join(os.getcwd(), "data/processed/task_A_BERT_output_pretrained_on_A.jsonl")
    with jsonlines.open(bert_output_path) as reader:
        for line in reader:
            cls_token = line['features'][0]
            cls_token_embedding = cls_token['layers'][0]['values']  # index 0 means these are from the last layer
            bert_features_list.append(cls_token_embedding)

    bert_features_df = pd.DataFrame(bert_features_list)
    
    task_A_training_data_sentences = pd.read_csv(os.path.join(os.getcwd(), "data/processed/task_A_training_data_sentences.csv"))
    bert_features_df['post_id'] = task_A_training_data_sentences['post_id']
    bert_features_df['sentence_num'] = task_A_training_data_sentences['sentence_num']
    
    cols = bert_features_df.columns.tolist()
    cols = cols[-2:] + cols[:-2]
    bert_features_df = bert_features_df[cols]
    bert_features_df.to_csv(bert_features_csv_path, index = False)
    
    
def aggregate_BERT_features_per_post_separate_csv_files():

    """
    The bert_features.csv files has the following columns:
    post_id / sentence_num / 768 columns as featuresa

    In this function, I will use the post_id to pick the sentences that belong to the same post
    and aggregate the corresponding 786 features.
    I will use min, max, and mean.

    I save them in separate csv files.
    Therefore, there will be 3 csv files with 768 + 1 columns and #rows == #posts

    """
    bert_features_per_post_mean_path = os.path.join(os.getcwd(), "data/processed/task_A_BERT_features_per_post_mean.csv")
    bert_features_per_post_min_path = os.path.join(os.getcwd(), "data/processed/task_A_BERT_features_per_post_min.csv")
    bert_features_per_post_max_path = os.path.join(os.getcwd(), "data/processed/task_A_BERT_features_per_post_max.csv")

    bert_features_per_sentence = pd.read_csv(os.path.join(os.getcwd(), "data/processed/task_A_BERT_features_per_sentence.csv"))

    bert_features_per_sentence = bert_features_per_sentence.drop(columns= ['sentence_num'])

    bert_features_per_post_mean = bert_features_per_sentence.groupby(['post_id']).mean()
    bert_features_per_post_min = bert_features_per_sentence.groupby(['post_id']).min()
    bert_features_per_post_max = bert_features_per_sentence.groupby(['post_id']).max()

    bert_features_per_post_mean.to_csv(bert_features_per_post_mean_path, index=False)
    bert_features_per_post_min.to_csv(bert_features_per_post_min_path, index=False)
    bert_features_per_post_max.to_csv(bert_features_per_post_max_path, index=False)



def aggregate_BERT_features_per_post():

    """
    The bert_features.csv files has the following columns:
    post_id / sentence_num / 768 columns as featuresa

    In this function, I will use the post_id to pick the sentences that belong to the same post
    and aggregate the corresponding 786 features.
    I will use min, max, and mean.

    I am saving them all in one csv file.
    Therefore, there will be 1 csv file with 1 + (768 *3) columns and #rows == #posts
    """

    bert_features_per_post_path = os.path.join(os.getcwd(), "data/processed/task_A_BERT_features_per_post_pretrained_on_A.csv")
    bert_features_per_sentence = pd.read_csv(os.path.join(os.getcwd(), "data/processed/task_A_BERT_features_per_sentence_pretrained_on_A.csv"))

    bert_features_per_sentence = bert_features_per_sentence.drop(columns= ['sentence_num'])

    bert_features_per_post = flatten_cols(bert_features_per_sentence.groupby(['post_id']).agg(['mean', 'max', 'min']))
    bert_features_per_post.to_csv(bert_features_per_post_path)


def aggregate_BERT_features_per_user():

    bert_features_per_user_path = os.path.join(os.getcwd(), "data/processed/task_A_BERT_features_per_user.csv")
    bert_features_per_sentence = pd.read_csv(os.path.join(os.getcwd(), "data/processed/task_A_BERT_features_per_sentence.csv"))
    bert_features_per_sentence = bert_features_per_sentence.drop(columns=['sentence_num'])

    task_A_train_posts = pd.read_csv(os.path.join(os.getcwd(), "data/processed/task_A_train_posts.csv"))

    # add the user_id column
    bert_features_per_sentence= pd.merge(bert_features_per_sentence, task_A_train_posts[['post_id', 'user_id']],
                                      on=['post_id'], how='left')

    # change the order of the columns to make user_id the first
    cols = bert_features_per_sentence.columns.tolist()
    cols = cols[-1:] + cols[:-1]
    bert_features_per_sentence = bert_features_per_sentence[cols]


    bert_features_per_user = flatten_cols(bert_features_per_sentence.groupby(['user_id']).agg(['mean', 'max', 'min']))
    bert_features_per_user.to_csv(bert_features_per_user_path)




if __name__ == "__main__":

    
    
    #training_data_sentences = get_training_data_sentences()
    #training_data_sentences = training_data_sentences[:10]
    #convert_training_sentences_to_txt()
    #convert_task_A_sentences_to_txt()


    
    extract_BERT_features()
    
    #process_BERT_features()
    #aggregate_BERT_features_per_post()



